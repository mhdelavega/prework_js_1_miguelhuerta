//Los mensajes que se muestren por consola deben ser siempre correctos sin importar cuáles 
//sean los números 1 y 2

function miFuncionDeComparar(numero1, numero2) {

    if (numero1 === 5) {
        console.log("numero1 es estrictamente igual a 5");
    }

    if (numero1 <= numero2) {
        console.log("numero1 no es mayor que numero2");
    }
    if (numero2 >= 0) {
        console.log("numero2 es positivo");
    }
    if (numero1 < 0) {
        console.log("numero1 es negativo o distinto de cero");
    }
}

miFuncionDeComparar(0, 0);
